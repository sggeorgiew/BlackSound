﻿using System;
using System.Collections.Generic;
using BlackSound.Entities;
using BlackSound.Repositories;
using BlackSound.Helpers;

namespace BlackSound.Views
{
    public sealed class SongManagementView : BaseView<Song>
    {
        protected override BaseRepository<Song> CreateRepository()
        {
            return new SongRepository();
        }

        protected override void RenderItem(Song song)
        {
            ArtistRepository artistRepo = new ArtistRepository();
            Artist artist = artistRepo.GetByID(song.ArtistID);

            Console.WriteLine("ID: {0}", song.ID);
            Console.WriteLine("Title: {0}", song.Title);
            Console.WriteLine("Artist: {0}", artist.Name);
            Console.WriteLine("Year: {0}", song.Year);
            Console.WriteLine(new string('=', 25));
        }

        protected override void UpdateOrAddItem(Song song)
        {
            ArtistRepository artistRepo = new ArtistRepository();
            List<Artist> artists = artistRepo.GetAll();

            if (artists.Count < 1)
            {
                throw new Exception("There are no artists available! Please add some.");
            }

            Console.WriteLine();
            Console.WriteLine(new string('=', 25));
            ConsoleMessage.Show("Available Artists\n\n", ConsoleColor.DarkCyan);

            foreach (Artist artist in artists)
            {
                Console.WriteLine("#{0} {1}", artist.ID, artist.Name);
            }

            Console.WriteLine(new string('=', 25));
            Console.WriteLine();

            // Update 
            if (song.ID != 0)
            {
                Console.Write("New Title: ");
                string title = Console.ReadLine();

                Console.Write("New ArtistID: ");
                string artistInput = Console.ReadLine();

                Console.Write("New Year: ");
                string yearInput = Console.ReadLine();

                Console.WriteLine(new string('=', 25));

                if (!string.IsNullOrEmpty(title))
                {
                    song.Title = title;
                }

                if (!string.IsNullOrEmpty(artistInput))
                {
                    if (!int.TryParse(artistInput, out int artistID))
                    {
                        throw new FormatException("Incorrect input! Please enter a number for ArtistID.");
                    }

                    Artist artist = artistRepo.GetByID(artistID);

                    if (artist == null)
                    {
                        throw new NullReferenceException($"We couldn't find an artist with id {artistID}");
                    }

                    song.ArtistID = artistID;
                }

                if (!string.IsNullOrEmpty(yearInput))
                {
                    if (!short.TryParse(yearInput, out short year))
                    {
                        throw new FormatException("Incorrect input! Please enter a correct year.");
                    }

                    song.Year = year;
                }
            }
            else
            {
                Console.Write("Title: ");
                song.Title = Console.ReadLine();

                Console.Write("ArtistID: ");
                if (!int.TryParse(Console.ReadLine(), out int artistID))
                {
                    throw new FormatException("Incorrect input! Please enter a number for ArtistID.");
                }

                Artist artist = artistRepo.GetByID(artistID);

                if (artist == null)
                {
                    throw new NullReferenceException($"We couldn't find an artist with id {artistID}");
                }

                song.ArtistID = artistID;

                Console.Write("Year: ");

                if (!short.TryParse(Console.ReadLine(), out short year))
                {
                    throw new FormatException("Incorrect input! Please enter a correct year.");
                }

                song.Year = year;

                Console.WriteLine(new string('=', 25));

                ValidateEntity(song);
            }
        }

    }
}
