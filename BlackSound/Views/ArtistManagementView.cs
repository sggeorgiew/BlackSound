﻿using System;
using BlackSound.Entities;
using BlackSound.Repositories;

namespace BlackSound.Views
{
    public sealed class ArtistManagementView : BaseView<Artist>
    {
        protected override BaseRepository<Artist> CreateRepository()
        {
            return new ArtistRepository();
        }

        protected override void RenderItem(Artist artist)
        {
            Console.WriteLine("ID: {0}", artist.ID);
            Console.WriteLine("Name: {0}", artist.Name);
            Console.WriteLine(new string('=', 25));
        }

        protected override void UpdateOrAddItem(Artist artist)
        {
            if (artist.ID != 0)
            {
                Console.Write("New Name: ");
                string name = Console.ReadLine();

                Console.WriteLine(new string('=', 25));

                if (!string.IsNullOrEmpty(name))
                {
                    artist.Name = name;
                }
            }
            else
            {
                Console.Write("Name: ");
                artist.Name = Console.ReadLine();

                if (string.IsNullOrEmpty(artist.Name))
                {
                    throw new ArgumentNullException("The artist name can't be empty!");
                }

                ArtistRepository artistRepo = new ArtistRepository();

                if (artistRepo.CheckArtistExists(artist.Name))
                {
                    throw new Exception("This artist already exists!");
                }

                Console.WriteLine(new string('=', 25));
            }
        }

    }
}
