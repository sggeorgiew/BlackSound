﻿using System;
using BlackSound.Entities;
using BlackSound.Repositories;

namespace BlackSound.Views
{
    public sealed class UserManagementView : BaseView<User>
    {
        protected override BaseRepository<User> CreateRepository()
        {
            return new UserRepository();
        }

        protected override void RenderItem(User user)
        {
            Console.WriteLine("ID: {0}", user.ID);
            Console.WriteLine("Email: {0}", user.Email);
            Console.WriteLine("Password: {0}", user.Password);
            Console.WriteLine("Display Name: {0}", user.DisplayName);
            Console.WriteLine("Admin privileges: {0}", user.IsAdmin);
            Console.WriteLine(new string('=', 25));
        }

        protected override void UpdateOrAddItem(User user)
        {
            if (user.ID != 0)
            {
                Console.Write("New Email: ");
                string email = Console.ReadLine();

                Console.Write("New Password: ");
                string password = Console.ReadLine();

                Console.Write("New Display Name: ");
                string displayName = Console.ReadLine();

                Console.Write("Admin privileges (True/False): ");
                string isAdminString = Console.ReadLine();

                Console.WriteLine(new string('=', 25));

                if (!string.IsNullOrEmpty(email))
                {
                    user.Email = email;
                }

                if (!string.IsNullOrEmpty(password))
                {
                    user.Password = password;
                }

                if (!string.IsNullOrEmpty(displayName))
                {
                    user.DisplayName = displayName;
                }

                if (!string.IsNullOrEmpty(isAdminString))
                {
                    if (!bool.TryParse(isAdminString, out bool isAdmin))
                    {
                        throw new FormatException("Incorrect input! Please enter true or false.");
                    }

                    user.IsAdmin = isAdmin;
                }
            }
            else
            {
                Console.Write("Email: ");
                user.Email = Console.ReadLine();

                Console.Write("Password: ");
                user.Password = Console.ReadLine();

                Console.Write("Display Name: ");
                user.DisplayName = Console.ReadLine();

                Console.Write("Admin privileges (True/False): ");
                string isUserAdmin = Console.ReadLine();

                if (!bool.TryParse(isUserAdmin, out bool isAdmin))
                {
                    throw new FormatException("Incorrect input! Please enter true or false.");
                }

                user.IsAdmin = isAdmin;

                Console.WriteLine(new string('=', 25));

                ValidateEntity(user);
            }
        }

    }
}
