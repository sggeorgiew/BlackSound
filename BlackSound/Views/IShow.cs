﻿using System;

namespace BlackSound.Views
{
    public interface IShow
    {
        void Show();
    }
}
