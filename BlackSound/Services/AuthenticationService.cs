﻿using System;
using System.Linq;
using BlackSound.Entities;
using BlackSound.Repositories;

namespace BlackSound.Services
{
    public static class AuthenticationService
    {
        public static User LoggedUser { get; private set; }

        public static void Authenticate(string email, string password)
        {
            UserRepository userRepo = new UserRepository();
            AuthenticationService.LoggedUser = userRepo.GetAll(u => u.Email == email && u.Password == password).FirstOrDefault();
        }
    }
}
