﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlackSound.Entities;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class ArtistRepository : BaseRepository<Artist>
    {
        public ArtistRepository(string filePath = Constants.ArtistsFilePath) : base(filePath)
        { }

        public bool CheckArtistExists(string artistName)
        {
            return (GetAll(a => a.Name == artistName).Any());
        }

        public override bool Delete(Artist artist)
        {
            SongRepository songRepo = new SongRepository();
            List<Song> songs = songRepo.GetAll(x => x.ArtistID == artist.ID);

            foreach (Song song in songs)
            {
                songRepo.Delete(song);
            }

            return base.Delete(artist);
        }
    }
}
