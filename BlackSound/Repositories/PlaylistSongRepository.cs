﻿using System;
using BlackSound.Entities;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class PlaylistSongRepository : BaseRepository<PlaylistSong>
    {
        public PlaylistSongRepository(string filePath = Constants.PlaylistSongsFilePath) : base(filePath)
        { }
    }
}
