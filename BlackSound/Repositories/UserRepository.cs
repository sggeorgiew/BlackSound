﻿using System;
using System.Linq;
using BlackSound.Entities;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(string filePath = Constants.UsersFilePath) : base(filePath)
        { }

        public bool CheckUserExists(string email, string displayName)
        {
            return (GetAll(u => u.Email == email || u.DisplayName == displayName).Any());
        }

        public override bool Delete(User user)
        {
            PlaylistRepository playlistRepo = new PlaylistRepository();
            var userPlaylists = playlistRepo.GetAll(x => x.UserID == user.ID);

            foreach (var playlist in userPlaylists)
            {
                playlistRepo.Delete(playlist);
            }

            return base.Delete(user);
        }
    }
}
