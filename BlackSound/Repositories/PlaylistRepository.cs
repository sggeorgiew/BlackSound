﻿using System;
using BlackSound.Entities;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class PlaylistRepository : BaseRepository<Playlist>
    {
        public PlaylistRepository(string filePath = Constants.PlaylistsFilePath) : base(filePath)
        { }

        public override bool Delete(Playlist playlist)
        {
            UserPlaylistRepository userPlaylistRepo = new UserPlaylistRepository();
            var userSharedPlaylists = userPlaylistRepo.GetAll(x => x.PlaylistID == playlist.ID);

            foreach (var userPlaylist in userSharedPlaylists)
            {
                userPlaylistRepo.Delete(userPlaylist);
            }

            PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository();
            var playlists = playlistSongRepo.GetAll(x => x.PlaylistID == playlist.ID);

            foreach (var playlistSong in playlists)
            {
                playlistSongRepo.Delete(playlistSong);
            }

            return base.Delete(playlist);
        }
    }
}
