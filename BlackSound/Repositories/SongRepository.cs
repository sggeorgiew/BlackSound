﻿using System;
using BlackSound.Entities;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class SongRepository : BaseRepository<Song>
    {
        public SongRepository(string filePath = Constants.SongsFilePath) : base(filePath)
        { }

        public override bool Delete(Song song)
        {
            PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository();
            var playlists = playlistSongRepo.GetAll(x => x.SongID == song.ID);

            foreach (var playlist in playlists)
            {
                playlistSongRepo.Delete(playlist);
            }

            return base.Delete(song);
        }
    }
}
