﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entities
{
    [DataContract]
    public class User : IEntity
    {
        [DataMember(Order = 1)]
        public int ID { get; set; }

        [DataMember(Order = 2)]
        public string Email { get; set; }

        [DataMember(Order = 3)]
        public string Password { get; set; }

        [DataMember(Order = 4)]
        public string DisplayName { get; set; }

        [DataMember(Order = 5)]
        public bool IsAdmin { get; set; }
    }
}
