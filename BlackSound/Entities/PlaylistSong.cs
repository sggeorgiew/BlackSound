﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entities
{
    [DataContract]
    public sealed class PlaylistSong : IEntity
    {
        [DataMember(Order = 1)]
        public int ID { get; set; }

        [DataMember(Order = 2)]
        public int PlaylistID { get; set; }

        [DataMember(Order = 3)]
        public int SongID { get; set; }
    }
}
