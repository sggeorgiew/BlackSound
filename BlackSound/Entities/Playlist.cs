﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Helpers;
using BlackSound.Repositories;

namespace BlackSound.Entities
{
    [DataContract]
    public class Playlist : IEntity
    {
        [DataMember(Order = 1)]
        public int ID { get; set; }

        [DataMember(Order = 2)]
        public int UserID { get; set; }

        [DataMember(Order = 3)]
        public string Name { get; set; }

        [DataMember(Order = 4)]
        public string Description { get; set; }

        [DataMember(Order = 5)]
        public bool IsPublic { get; set; }

        public List<User> SharedWith()
        {
            UserPlaylistRepository userPlaylistRepo = new UserPlaylistRepository();
            var userPlaylists = userPlaylistRepo.GetAll(x => x.PlaylistID == this.ID);

            UserRepository userRepo = new UserRepository();
            List<User> users = new List<User>();

            foreach (var userPlaylist in userPlaylists)
            {
                users.Add(userRepo.GetByID(userPlaylist.UserID));
            }

            return users;
        }

        public bool IsSharedWith(int userID)
        {
            UserPlaylistRepository userPlaylistRepo = new UserPlaylistRepository();
            bool isShared = userPlaylistRepo.GetAll(x => x.PlaylistID == this.ID && x.UserID == userID).Any();

            return (this.UserID == userID || isShared || this.IsPublic);
        }
    }
}
